# Exercise - Build And Run With Commandline

## 1. Clone the repository to Your Linux Machine

On your linux machine clone the repo:
```git clone https://bitbucket.org/fcallaly/northwind-shippers-demo.git```

cd into the cloned directory
```cd northwind-shippers-demo```

## 2. Test & build the java code

If the maven wrapper is not executable then make it so: ```chmod a+x mvnw```

Use maven to build a jar - note this will also run all of the unit tests in the code
```./mvnw clean package```

## 3. (Optional) Run the jar with the h2 profile

This profile is for testing and development.

This profile creates the database server INSIDE the java program, so it doesn't require database username, password or hostname options.

Using this database INSIDE the java program means that the database schema is automatically installed.

Using this database INSIDE the java program is only really useful for testing and development.

To use the H2 database, we need to set some properties to e.g. - use the h2 profile, use port 8081

e.g.
```java -DSERVER_PORT=8081 -DSPRING_PROFILE=h2 -jar target/northwind-shippers-api-0.0.1-SNAPSHOT.jar```

Take a look at the application.properties file, and try to understand the mapping between the above properties being set and the placeholders in application.properties

Verify the program is working by going to the swagger ui at YOUR LINUX HOSTNAME on port 8081 from any web browser:
e.g. ```http://YOUR_LINUX_HOSTNAME:8081```


What happens if you add a new database record through the POST method on the swagger ui and then restart the app?

Is your new shipper still there? Why/Why Not?

## 4. (Optional) Run the jar with the mysql profile

In this case we first need to put the schema into the mysql database:
```
# log in to mysql
mysql -u root -pC0nygre-C0nygre

# create a database called Northwind
create database Northwind;

# log out of mysql
exit

# Now load the schema.sql file into your newly created database
mysql -u root -pC0nygre-C0nygre Northwind < src/main/resources/schema.sql
```

To run the jar so that it uses the mysql database you will need to set some properties e.g. - use port 8081, set the mysql user/pasword

Note in this case we're setting the properties as env variables in the shell. This is an alternative method to the "-D...." fcawe used above.

e.g.
```
export SERVER_PORT=8081
export DB_USER=conygre
export DB_PASS=C0nygre-C0nygre
java -jar target/northwind-shippers-api-0.0.1-SNAPSHOT.jar
```

Same as we did for the h2 profile:

Verify the program is working by going to the swagger ui at YOUR LINUX HOSTNAME on port 8081 from any web browser:
e.g. ```http://YOUR_LINUX_HOSTNAME:8081```


What happens if you add a new database record through the POST method on the swagger ui and then restart the app?

Is your new shipper still there? Why/Why Not?