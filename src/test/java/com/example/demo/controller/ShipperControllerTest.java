package com.example.demo.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.example.demo.entities.Shipper;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;


@ActiveProfiles("h2")
@SpringBootTest
@AutoConfigureMockMvc
public class ShipperControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void testShouldReturnListSize3() throws Exception {
		// 1. setup stuff
		
		
		// 2. call method under test
		MvcResult mvcResult = this.mockMvc.perform(get("/api/shippers"))
							              .andDo(print())
							              .andExpect(status().isOk())
							              .andReturn();
		
		// 3. verify the results
		List<Shipper> shippers = new ObjectMapper().readValue(
											mvcResult.getResponse().getContentAsString(),
				                            new TypeReference<List<Shipper>>() { });
		
		assertThat(shippers.size()).isGreaterThan(0);
	}
	
	@Test
	public void testFindByIdSuccess() throws Exception {
		// 1. setup stuff
		int testId = 1;
		
		// 2. call method under test
		MvcResult mvcResult = this.mockMvc.perform(get("/api/shippers/" +  testId))
							              .andDo(print())
							              .andExpect(status().isOk())
							              .andReturn();
		
		// 3. verify the results
		Shipper shippers = new ObjectMapper().readValue(
											mvcResult.getResponse().getContentAsString(),
				                            new TypeReference<Shipper>() { });
		
		assertThat(shippers.getId()).isEqualTo(testId);
	}
	
	@Test
	public void testFindByIdFailure() throws Exception {
		// 1. setup stuff
		
		
		// 2. call method under test
		this.mockMvc.perform(get("/api/shippers/9999"))
					.andDo(print())
					.andExpect(status().isNotFound());
	}
	
	@Test
	public void testCreateShipper() throws Exception {
		// 1. setup stuff
		String testName = "Frank";
		String testPhone = "091 444333";
		
		Shipper testShipper = new Shipper();
        testShipper.setName(testName);
        testShipper.setPhone(testPhone);
        
		ObjectMapper mapper = new ObjectMapper();
    	ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson=ow.writeValueAsString(testShipper);
		
		System.out.println(requestJson);

		// 2. call method under test
		MvcResult mvcResult = this.mockMvc.perform(post("/api/shippers/")
										  			.header("Content-Type", "application/json")
										  			.content(requestJson))
								.andDo(print()).andExpect(status().isOk()).andReturn();
		
		// 3. verify the results
		Shipper shippers = new ObjectMapper().readValue(
				mvcResult.getResponse().getContentAsString(),
                new TypeReference<Shipper>() { });

		assertThat(shippers.getName()).isEqualTo(testName);
		assertThat(shippers.getPhone()).isEqualTo(testPhone);
	}
	
	@Test
	public void testDeleteShipper() throws Exception {
		// 1. setup stuff
		int testId = 2;

		// 2. call method under test
		MvcResult mvcResult = this.mockMvc.perform(delete("/api/shippers/" + testId)
										  			.header("Content-Type", "application/json"))
								.andDo(print()).andExpect(status().isOk()).andReturn();
		
		// 3. verify the results
		int resultId = new ObjectMapper().readValue(
				mvcResult.getResponse().getContentAsString(),
                new TypeReference<Integer>() { });

		assertThat(testId).isEqualTo(resultId);
		
		// ensure this id is not found now
		this.mockMvc.perform(get("/api/shippers/" + testId))
							.andDo(print())
							.andExpect(status().isNotFound());
	}
	
	@Test
	public void testEditShipper() throws Exception {
		// 1. setup stuff
		int testId = 1;
		String testName = "Frank Edit";
		String testPhone = "091 444333 9999";
		
		Shipper testShipper = new Shipper();
		testShipper.setId(testId);
        testShipper.setName(testName);
        testShipper.setPhone(testPhone);
        
		ObjectMapper mapper = new ObjectMapper();
    	ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson=ow.writeValueAsString(testShipper);
		
		System.out.println(requestJson);

		// 2. call method under test
		MvcResult mvcResult = this.mockMvc.perform(put("/api/shippers/")
										  			.header("Content-Type", "application/json")
										  			.content(requestJson))
								.andDo(print()).andExpect(status().isOk()).andReturn();
		
		// 3. verify the results
		Shipper shipper = new ObjectMapper().readValue(
				mvcResult.getResponse().getContentAsString(),
                new TypeReference<Shipper>() { });

		assertThat(shipper.getName()).isEqualTo(testName);
		assertThat(shipper.getPhone()).isEqualTo(testPhone);
		
		// verify the edit took place
		mvcResult = this.mockMvc.perform(get("/api/shippers/" +  testId))
	              .andDo(print())
	              .andExpect(status().isOk())
	              .andReturn();


		shipper = new ObjectMapper().readValue(
					mvcResult.getResponse().getContentAsString(),
					new TypeReference<Shipper>() { });

		assertThat(shipper.getId()).isEqualTo(testId);
		assertThat(shipper.getName()).isEqualTo(testName);
		assertThat(shipper.getPhone()).isEqualTo(testPhone);
	}
}
